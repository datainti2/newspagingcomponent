import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { BaseWidgetComponent } from './src/base-widget/base-widget.component';

export * from './src/base-widget/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
	MaterialModule
  ],
  declarations: [
	BaseWidgetComponent
  ],
  exports: [
    BaseWidgetComponent
  ]
})
export class NewsPagingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NewsPagingModule,
      providers: []
    };
  }
}
